--- Persona
ALTER TABLE `nilo`.persona
ADD CONSTRAINT `cnt_tipodocumento_persona`
FOREIGN KEY (fk_tipoiId) REFERENCES `nilo`.`tipoID`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- ALTER TABLE `nilo`.persona
-- ADD CONSTRAINT `cnt_tipopersona_persona`
-- FOREIGN KEY (fk_tipopersona) REFERENCES `nilo`.`tipopersona`(`pk_nombre`) 
-- ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.persona
ADD CONSTRAINT uc_id UNIQUE (documento, fk_tipoiId, estado);

ALTER TABLE `nilo`.persona
ADD CONSTRAINT uc_email UNIQUE (email);

ALTER TABLE `nilo`.persona
ADD CONSTRAINT uc_usuario UNIQUE (usuario);

--- Organismo
ALTER TABLE `nilo`.organismo
ADD CONSTRAINT `cnt_representantelegal_organismo`
FOREIGN KEY (fk_representante_legal) REFERENCES `nilo`.`persona`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.organismo
ADD CONSTRAINT `cnt_tipoID_organismo`
FOREIGN KEY (fk_tipoiId) REFERENCES `nilo`.`tipoID`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.organismo
ADD CONSTRAINT `cnt_tipocliente_organismo`
FOREIGN KEY (fk_tipocliente) REFERENCES `nilo`.`tipocliente`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.organismo
ADD CONSTRAINT uc_documento_organismo UNIQUE (documento, fk_tipoiId);

-- abogado
ALTER TABLE `nilo`.abogado
ADD CONSTRAINT `cnt_fk_persona_abogado`
FOREIGN KEY (fk_persona) REFERENCES `nilo`.`persona`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.abogado
ADD CONSTRAINT uc_nombre_abogado UNIQUE (fk_persona, estado);


--- proceso
ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_organismo_proceso`
FOREIGN KEY (fk_organismo) REFERENCES `nilo`.`organismo`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- ALTER TABLE `nilo`.proceso
-- ADD CONSTRAINT `cnt_abogado_proceso`
-- FOREIGN KEY (fk_abogadoasignado) REFERENCES `nilo`.`abogado`(`id`) 
-- ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_accionpenal_proceso`
FOREIGN KEY (fk_tipoaccion) REFERENCES `nilo`.`tipoaccionpenal`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_tipocaso_proceso`
FOREIGN KEY (fk_tipocaso) REFERENCES `nilo`.`tipocaso`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_delito_proceso`
FOREIGN KEY (fk_delito) REFERENCES `nilo`.`delito`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_fiscalia_proceso`
FOREIGN KEY (fk_fiscalia_actual) REFERENCES `nilo`.`fiscalia`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_etapa_proceso`
FOREIGN KEY (fk_etapa_proceso) REFERENCES `nilo`.`etapa_proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT `cnt_vehiculo_proceso`
FOREIGN KEY (fk_placa) REFERENCES `nilo`.`vehiculo`(`pk_placa`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.proceso
ADD CONSTRAINT uc_radicado_proceso UNIQUE (radicado, sinistro ,tramite);

-- Fiscal
ALTER TABLE `nilo`.fiscal
ADD CONSTRAINT `cnt_fiscalia_fiscal`
FOREIGN KEY (fk_fiscalia) REFERENCES `nilo`.`fiscalia`(`pk_nombre`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.fiscal
ADD CONSTRAINT `cnt_persona_fiscal`
FOREIGN KEY (fk_persona) REFERENCES `nilo`.`persona`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.fiscal
ADD CONSTRAINT uc_nombre_abogado UNIQUE (fk_fiscalia, estado);

-- etapa_proceso
ALTER TABLE `nilo`.etapa_proceso
ADD CONSTRAINT uc_nombre_etapa_proceso UNIQUE (etapa, nombre);

-- abogado_proceso
ALTER TABLE `nilo`.abogado_proceso
ADD CONSTRAINT `cnt_proceso_abogadoproceso`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.abogado_proceso
ADD CONSTRAINT `cnt_abogado_abogadoproceso`
FOREIGN KEY (fk_abogado) REFERENCES `nilo`.`abogado`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;


-- denuncia_proceso
ALTER TABLE `nilo`.denuncia_proceso
ADD CONSTRAINT `cnt_proceso_denunciaproceso`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.denuncia_proceso
ADD CONSTRAINT `cnt_persona_denunciaproceso`
FOREIGN KEY (fk_persona) REFERENCES `nilo`.`persona`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- defensa_proceso
ALTER TABLE `nilo`.defensa_proceso
ADD CONSTRAINT `cnt_proceso_defensaproceso`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.defensa_proceso
ADD CONSTRAINT `cnt_persona_defensaproceso`
FOREIGN KEY (fk_persona) REFERENCES `nilo`.`persona`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;


-- hoja_ruta
ALTER TABLE `nilo`.hoja_ruta
ADD CONSTRAINT `cnt_caso_hojaruta`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- actuacion_procesal

ALTER TABLE `nilo`.actuacion_procesal
ADD CONSTRAINT `cnt_proceso_actuacionprocesal`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.actuacion_procesal
ADD CONSTRAINT `cnt_hojaruta_actuacionprocesal`
FOREIGN KEY (fk_hojaruta) REFERENCES `nilo`.`hoja_ruta`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.actuacion_procesal
ADD CONSTRAINT `cnt_abogado_actuacionprocesal`
FOREIGN KEY (fk_abogado) REFERENCES `nilo`.`abogado`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE `nilo`.actuacion_procesal
ADD CONSTRAINT `cnt_documento_actuacionprocesal`
FOREIGN KEY (fk_documento) REFERENCES `nilo`.`documento`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- relacion de horas
ALTER TABLE `nilo`.relacion_horas
ADD CONSTRAINT `cnt_abogado_relacionhoras`
FOREIGN KEY (fk_abogado) REFERENCES `nilo`.`abogado`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.relacion_horas
ADD CONSTRAINT `cnt_proceso_relacionhoras`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

-- documento

ALTER TABLE `nilo`.documento
ADD CONSTRAINT uc_code_documento UNIQUE (codigo);


-- agenda
ALTER TABLE `nilo`.agenda
ADD CONSTRAINT `cnt_proceso_agenda`
FOREIGN KEY (fk_proceso) REFERENCES `nilo`.`proceso`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `nilo`.agenda
ADD CONSTRAINT `cnt_abogado_agenda`
FOREIGN KEY (fk_abogado) REFERENCES `nilo`.`abogado`(`id`) 
ON UPDATE CASCADE ON DELETE CASCADE;
