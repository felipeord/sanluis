-- Persona
CREATE TABLE `nilo`.`persona` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) NOT NULL,
  `usuario` varchar(16) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `fk_tipoiId` varchar(20) NOT NULL,
  `documento` varchar(30) NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'ACTIVO', -- ACTIVO | INACTIVO | SUSPENDIDO
  `pais` varchar(40) DEFAULT '',
  `ciudad` varchar(40) DEFAULT '',
  `direccion` varchar(50) DEFAULT '',
  `telefono` bigint(10),
  `celular` bigint(10),
  `extension` varchar(10) DEFAULT '',
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representa los usuarios del sistema.';


-- tipodocumento
CREATE TABLE `nilo`.`tipoID` (
  `pk_nombre` varchar(20) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descripcion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipo de identificacion de un documento (nit|cc|ti).';

-- organismo
CREATE TABLE `nilo`.`organismo` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `estado` varchar(20) NOT NULL DEFAULT 'activo', -- activo|inactivo
  `nombre` varchar(80) NOT NULL,
--  `array_personas` varchar(100) DEFAULT '',
  `fk_representante_legal` int(9) NOT NULL,
  `fk_tipoiId` varchar(20) NOT NULL,
  `documento` varchar(30),
  `fk_tipocliente` varchar(20) NOT NULL, -- Cliente|alido|proovedor
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipos de organismos (cliente|proveedor|aliado|usuario|acusado|victima|defensa|testigo|juez).';


-- tipocliente
CREATE TABLE `nilo`.`tipocliente` (
  `pk_nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipos de organismo (cliente|proveedor|aliado|usuario|acusado|victima|defensa|testigo|juez).';

-- abogado
CREATE TABLE `nilo`.`abogado` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `fk_persona` int(9) NOT NULL,
  `estado` VARCHAR(20) NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_modificacion` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los abogados de la organizaci�n .';

-- tipopersona`
CREATE TABLE `nilo`.`tipopersona` (
  `pk_nombre` varchar(20) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipos de persona .';


-- Caso 
CREATE TABLE `nilo`.`proceso` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `asegurador` varchar(60) NOT NULL, -- Asegurador
  `fk_organismo` int(9) NOT NULL, -- Asegurador
--  `fk_abogadoasignado` int(9) NOT NULL,
--  `fk_conductor` int(9) NOT NULL,
  `fk_tipoaccion` varchar(20) NOT NULL,
  `fk_tipocaso` varchar(20) NOT NULL,
  `fk_delito` varchar(90) NOT NULL,
  `fk_fiscalia_actual` varchar(90) NOT NULL,
  `fk_etapa_proceso` int(9) NOT NULL,
  `fk_placa` varchar(20),
  `radicado` varchar(30),
  `sinistro` varchar(30),
  `poliza` varchar(30),
  `tramite` varchar(30),
  `estado` varchar(20) NOT NULL, -- (activo|inactivo|en espera|aplazado|cerrado|re-abierto)
--  `fk_agenda` int(9) NOT NULL, -- Agenda
  `dt_actualizacion_sigal` timestamp,
  `dt_actualizacion_sp` timestamp,
  `dt_modificacion` timestamp,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar un caso.';


-- tipo accion (penal, administrativa, civil)
CREATE TABLE `nilo`.`tipoaccionpenal` (
  `pk_nombre` varchar(20) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descripcion` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipos de accion penal.';

-- tipocaso (defensa, denuncia)
CREATE TABLE `nilo`.`tipocaso` (
  `pk_nombre` varchar(20) NOT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los tipos de caso.';


-- delito 
CREATE TABLE `nilo`.`delito` (
  `pk_nombre` varchar(90) NOT NULL,
  `descripcion` varchar(400) DEFAULT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los delitos.';


-- fiscalia 
CREATE TABLE `nilo`.`fiscalia` (
  `pk_nombre` varchar(90) NOT NULL,
  `ciudad` varchar(90) NOT NULL,
  `direccion` varchar(90) NOT NULL,
  `telefono` bigint(10),
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_nombre`, `ciudad` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar las fiscalias.';

-- fiscal
CREATE TABLE `nilo`.`fiscal` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `fk_fiscalia` varchar(90) NOT NULL,
  `fk_persona` int(9) NOT NULL,
  `estado` VARCHAR(20) NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_modificacion` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar los fiscales del caso.';


-- Etapa del proceso 
CREATE TABLE `nilo`.`etapa_proceso` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `etapa` varchar(90) NOT NULL,
  `nombre` varchar(90) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar las fiscalias.';

-- Veh�culo
CREATE TABLE `nilo`.`vehiculo` (
  `pk_placa` varchar(10) NOT NULL,
  `dueno` varchar(90) DEFAULT '',
  `marca` varchar(20) DEFAULT '',
  `modelo` varchar(20) DEFAULT '',
  `anotaci�n` varchar(400) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pk_placa`, `dueno` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar un vehiculo.';

-- Caso abogado_proceso 1 a *
CREATE TABLE `nilo`.`abogado_proceso` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `fk_abogado` int(9) NOT NULL,
  `fk_proceso` int(9) NOT NULL, 
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de relacionar victimas con un proceso.';

-- Caso victima_proceso 1 a *
CREATE TABLE `nilo`.`denuncia_proceso` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `fk_proceso` int(9) NOT NULL, 
  `fk_persona` int(9) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de relacionar victimas con un proceso.';



-- Caso denunciantes_proceso 1 a *
CREATE TABLE `nilo`.`defensa_proceso` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `fk_proceso` int(9) NOT NULL, -- Asegurador
  `fk_persona` int(9) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de relacionar victimas con un proceso.';

-- Relacion horas
CREATE TABLE `nilo`.`relacion_horas` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `fk_abogado` int(9) NOT NULL,
  `fk_proceso` int(9) NOT NULL,
  `fk_agenda` int(9) NOT NULL,
  `duracion` int(11) NOT NULL, -- minutos
  `actuaci�n` varchar(400) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representa la relacion de horas de un abogado.';


-- Historia (Hoja de ruta) 
CREATE TABLE `nilo`.`hoja_ruta` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `fk_proceso` int(9) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representa las hojas de ruta.';


-- Relacion actuaciones procesales
CREATE TABLE `nilo`.`actuacion_procesal` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `fk_proceso` int(9) NOT NULL,
  `fk_hojaruta` int(9) NOT NULL,
  `fk_abogado` int(9) NOT NULL,
  `fk_documento` int(9) NOT NULL,
  `actuaci�n` varchar(400) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `dt_evento` timestamp NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representa las actuaciones procesales.';


-- Documentos  
CREATE TABLE `nilo`.`documento` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(90) NOT NULL,
  `carpeta` varchar(90) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de representar un documento.';



-- Agenda  
CREATE TABLE `nilo`.`agenda` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `fk_proceso` int(9) NOT NULL,
  `fk_abogado` int(9) NOT NULL,
  `lugar` int(9) ,
  `nombre` varchar(90) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `dt_evento` timestamp NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de registrar la agenda de eventos.';


-- ReportMNG
CREATE TABLE `nilo`.`reporte` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `code` varchar(20) NOT NULL,
  `columns` varchar(400) NOT NULL,
  `query` varchar(400) NOT NULL,
  `style` int(9) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de registrar la auditoria de cada proceso.';


-- Auditoria  
CREATE TABLE `nilo`.`auditoria` (
  `id` int(9) NOT NULL AUTO_INCREMENT ,
  `dominio` varchar(90) NOT NULL,
  `proceso` varchar(90) NOT NULL,
  `descripcion` varchar(90) NOT NULL,
  `fk_persona` varchar(80) NOT NULL,
  `dt_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
COMMENT='Tabla encargada de registrar la auditoria de cada proceso.';


-- 
