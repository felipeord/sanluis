
DROP TABLE IF EXISTS `nilo`.`persona`;
DROP TABLE IF EXISTS `nilo`.`tipopersona`;
DROP TABLE IF EXISTS `nilo`.`organismo`;
DROP TABLE IF EXISTS `nilo`.`tipocliente`;
DROP TABLE IF EXISTS `nilo`.`tipodocumento`;
DROP TABLE IF EXISTS `nilo`.`caso`;
DROP TABLE IF EXISTS `nilo`.`tipocaso`;
DROP TABLE IF EXISTS `nilo`.`delito`;
DROP TABLE IF EXISTS `nilo`.`fiscalia`;
DROP TABLE IF EXISTS `nilo`.`hoja_ruta`;
DROP TABLE IF EXISTS `nilo`.`relacion_horas`;
DROP TABLE IF EXISTS `nilo`.`auditoria`;

