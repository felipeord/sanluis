--- Persona

ALTER TABLE `nilo`.persona
DROP FOREIGN KEY `cnt_tipodocumento_persona`;

ALTER TABLE `nilo`.persona
DROP FOREIGN KEY `cnt_tipopersona_persona`;

ALTER TABLE `nilo`.persona
DROP INDEX  uc_documento;




--- Organismo
ALTER TABLE `nilo`.organismo
DROP FOREIGN KEY `cnt_tipodocumento_organismo`;

ALTER TABLE `nilo`.organismo
DROP FOREIGN KEY `cnt_tipocliente_organismo`;

ALTER TABLE `nilo`.organismo
DROP INDEX uc_documento_organismo;

--- Caso
ALTER TABLE `nilo`.caso
DROP FOREIGN KEY `cnt_organismo_caso`;

ALTER TABLE `nilo`.caso
DROP FOREIGN KEY `cnt_tipocaso_caso`;

ALTER TABLE `nilo`.caso
DROP FOREIGN KEY `cnt_delito_caso`;

ALTER TABLE `nilo`.caso
DROP FOREIGN KEY `cnt_fiscalia_caso`;

-- ALTER TABLE `nilo`.caso
-- ADD CONSTRAINT uc_documento_organismo UNIQUE (documento,fk_tipodocumento);


-- hoja_ruta
ALTER TABLE `nilo`.hoja_ruta
DROP FOREIGN KEY `cnt_caso_hojaruta`;

ALTER TABLE `nilo`.hoja_ruta
DROP FOREIGN KEY `cnt_abogado_hojaruta`;

-- relacion de horas
ALTER TABLE `nilo`.relacion_horas
DROP FOREIGN KEY `cnt_persona_relacionhoras`;

ALTER TABLE `nilo`.relacion_horas
DROP FOREIGN KEY `cnt_caso_relacionhoras`;

