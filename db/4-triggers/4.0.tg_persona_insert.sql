DROP TRIGGER IF EXISTS nilo.trg_persona_insert;

DELIMITER $$
CREATE  TRIGGER `nilo`.`trg_persona_insert` BEFORE INSERT ON nilo.persona FOR EACH ROW
BEGIN
   DECLARE codDoc VARCHAR(20);
   DECLARE v_date DATE;
   DECLARE maxCode INT;

   set v_resul = 0;
   IF NEW.estado NOT IN ('ACTIVO', 'INACTIVO', 'SUSPENDIDO') THEN
        signal sqlstate '45000' set message_text = 'Usuario estado incorrecto: ' + NEW.estado;
   END IF;


   END$$
DELIMITER ;
