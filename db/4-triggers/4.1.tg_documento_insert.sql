DROP TRIGGER IF EXISTS nilo.tg_documento_insert;

DELIMITER $$
CREATE  TRIGGER `nilo`.`tg_documento_insert` BEFORE INSERT ON nilo.documento FOR EACH ROW
BEGIN

   DECLARE codDoc VARCHAR(20);
   DECLARE v_date DATE;
   DECLARE maxCode INT;

   set v_resul = 0;
   IF NEW.carpeta NOT IN ('ACTUACIÓN PROCESAL', 'ANEXO PROCESO', 'INFORMES', 'OTROS', 'OTROS/EMAILS') THEN
        signal sqlstate '45000' set message_text = 'Carpeta incorrecta: ' + NEW.carpeta;
   END IF;

   -- Find max file
   SELECT
      max(1)
   INTO
      maxCode
   FROM nilo.documento
   WHERE carpeta = new.carpeta;

   -- Decode file name
   IF NEW.carpeta = 'ACTUACIÓN PROCESAL' THEN
      codDoc = 'AP';
   ELSEIF NEW.carpeta = 'ANEXO PROCESO' THEN
      codDoc = 'XP';
   ELSEIF NEW.carpeta = 'INFORMES' THEN
      codDoc = 'IN';
   ELSEIF NEW.carpeta = 'OTROS' THEN
      codDoc = 'OT';
   ELSEIF NEW.carpeta = 'OTROS/EMAILS' THEN
      codDoc = 'EM';
   END IF;

   -- Save new value
   SET NEW.codigo = 'EM' + maxCode;


   END$$
DELIMITER ;
