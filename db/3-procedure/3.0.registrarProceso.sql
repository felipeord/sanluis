DROP PROCEDURE IF EXISTS nilo.registrarProceso;

DELIMITER $$
CREATE  PROCEDURE nilo.`registrarProceso`(IN nm_asegurador VARCHAR(200),
                                                IN id_organismo INT, -- Asegurado
                                                IN id_conductor INT,
                                                IN id_abogado INT,
                                                IN nm_accionpenal VARCHAR(20),
                                                IN nm_tipocaso VARCHAR(20),
                                                IN nm_delito VARCHAR(90),
                                                IN nm_fiscalia VARCHAR(90),
                                                IN id_placa VARCHAR(10),
                                                IN v_radicado VARCHAR(30),
                                                IN v_siniestro VARCHAR(30),
                                                IN v_poliza VARCHAR(30),
                                                IN v_tramite VARCHAR(30),
                                                OUT result INT)
proc_registrarProceso: BEGIN


  IF (v_radicado = '' OR v_siniestro = '' OR v_poliza = '' OR v_tramite = '' )  THEN
    SET result = 101; -- Empty fields
    LEAVE proc_registrarProceso;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM nilo.abogado WHERE id = id_abogado )  THEN
    SET result = 201; -- NOT conductor
    LEAVE proc_registrarProceso;
  END IF;
  
  IF NOT EXISTS (SELECT 1 FROM nilo.persona WHERE id = id_conductor )  THEN
    SET result = 202; -- NOT Abogado
    LEAVE proc_registrarProceso;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM nilo.tipoaccionpenal WHERE pk_nombre = nm_accionpenal )  THEN
    SET result = 301; -- NOT accion
    LEAVE proc_registrarProceso;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM nilo.tipocaso WHERE pk_nombre = nm_tipocaso )  THEN
    SET result = 302; -- NOT tipocaso !important
    LEAVE proc_registrarProceso;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM nilo.delito WHERE pk_nombre = nm_delito )  THEN
    SET result = 401; -- NOT delito
    LEAVE proc_registrarProceso;
  END IF;



  -- 1. Create fiscalia
  IF nm_fiscalia = '' THEN
     SET nm_fiscalia = 'NO ASIGNADA';
  END IF;

  IF NOT EXISTS (SELECT 1 FROM nilo.fiscalia WHERE pk_nombre = id_placa ) THEN

     INSERT INTO `nilo`.`vehiculo`(
       `pk_placa`,
       `dueno`,
       `marca`,
       `modelo`,
       `anotación`
     ) VALUES(
       id_placa,
       '',
       '',
       '',
       '';

   -- LAST_INSERT_ID()
  END IF;

   -- 2. Add vehículo
  IF NOT EXISTS (SELECT 1 FROM nilo.vehiculo WHERE pk_placa = nm_fiscalia ) THEN

     INSERT INTO `nilo`.`fiscalia`
        (`pk_nombre`,
        `ciudad`,
        `direccion`,
        `telefono`,
     )VALUES (
        nm_fiscalia,
        '',
        '',
        0;

   -- LAST_INSERT_ID()
  END IF;
   
   
   
   -- 3. Create fiscalia
   INSERT INTO `nilo`.`proceso` ( `asegurador`,
                                  `fk_organismo`,
                                  `fk_abogadoasignado`,
                                  `fk_tipoaccion`,
                                  `fk_tipocaso`,
                                  `fk_delito`, 
                                  `fk_fiscalia_actual`,
                                  `fk_etapa_proceso`,
                                  `fk_placa`,
                                  `radicado`,
                                  `sinistro`,
                                  `poliza`,
                                  `tramite`,
                                  `estado`)
                            VALUES
                                   nm_asegurador,
                                   id_organismo,
                                   id_abogado,
                                   nm_accionpenal,
                                   nm_tipocaso,
                                   nm_delito,
                                   nm_fiscalia,
                                   1, -- Nueva etapa asignación
                                   id_placa,
                                   v_radicado,
                                   v_siniestro,
                                   v_poliza,
                                   v_tramite
                                   );


  DECLARE _estado INT(20);
  DECLARE _puntoRetorno VARCHAR(45);
  -- OUT: 0 Succes | 1 Error | 201 NOT FOUND | 101 WRONG STATUS | 301 ERROR EXTERNAL PROCEDURE   
  SET result = 0;
    
  IF NOT EXISTS (SELECT 1 FROM bikes4free.bicicleta WHERE id = id_bicleta AND estado = 'libre'  )  THEN
    SET result = 201; -- NOT FOUND
    LEAVE proc_repararBicicleta;
  END IF;

  IF NOT EXISTS (SELECT 1 FROM bikes4free.usuario WHERE id = id_proveedor AND fk_tipousuario = 'mantenimiento' )  THEN
    SET result = 202; -- NOT FOUND
    LEAVE proc_repararBicicleta;
  END IF;


  UPDATE bikes4free.bicicleta
  SET estado = 'mantenimiento'
  where id = id_bicleta;
  
  INSERT INTO bikes4free.mantenimiento
  (fk_bicicleta, provedor_id, estado, detalle, dt_creacion) 
   VALUES 
  (id_bicleta, id_proveedor, 'activo', comentario, NOW() );
  
  

END$$
DELIMITER ;
