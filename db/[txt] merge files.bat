REM afp 20161007 merge files

@ECHO off
ECHO -----------------------------------------------
ECHO --------------------MERGE----------------------
ECHO -----------------------------------------------
ECHO Todos los archivos de una carpeta se combinaran 
ECHO los export son definidos son:
ECHO  - - - - - - - - - - - - - - - - - - - - - - - - - - - 
ECHO                  0 - sql
ECHO  - - - - - - - - - - - - - - - - - - - - - - - - - - - 

REM VARIABLES GLOBALES
set type=sql

REM fecha
FOR /f %%x in ('wmic path win32_localtime get /format:list ^| findstr "="') do set %%x
FOR /F "TOKENS=1,2 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET mm=%%B
if %Day% LSS 10 set day=0%Day%
SET comment=apolan created %Year%%mm%%Day%

rem goto read_log

:init_program

set pathFolder=%~dp0

cd %pathFolder%
set folder=%pathFolder%\7-combined

:loop
if not exist "%folder%" mkdir "%folder%"

SET target=%folder%\combined_%Year%%mm%%Day%.%type%
copy NUL "%target%"

for %%x in (
   1-init
   2-function
   3-procedure
   4-triggers
   5-data
) do (
   cd "%pathFolder%"\%%x
   FOR %%i IN (*) DO (
      @echo --  ----------------------------------- %comment% : %%i  >>  "%target%"
      copy "%target%"+%%i /b "%target%"
   )
)


move /Y "%target%" "%pathFolder%"
rd "%folder%"