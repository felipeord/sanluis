-- insert tipoID
INSERT INTO `nilo`.`tipoid` (`pk_nombre`,`descripcion`)
VALUES
( 'Tarjeta Identidad', 'Tarjeta identiad persona menor de edad' ),
( 'C�dula Ciudadan�a', 'C�dula Ciudadania persona mayor de edad' ),
( 'NIT', 'NIT' ),
( 'Pasaporte', 'Pasaporte extrajenro' );

-- insert tipoCliente
INSERT INTO `nilo`.`tipocliente`(`pk_nombre`,`descripcion`)
VALUES
('Cliente', 'Un cliente'),
('Aliado', 'Un aliado'),
('Proovedor', 'Un Proveedor');

-- insert tipoaccion
INSERT INTO `nilo`.`tipoaccionpenal(`pk_nombre`, `descripcion`)
VALUES
('Civil','DESC'),
('Penal','DESC'),
('Administrativa','DESC');

-- insert tipocaso
INSERT INTO `nilo`.`tipocaso`(`pk_nombre`, `descripcion`)
VALUES
('Denuncia',''),
('Defensa','');

-- insert delito
INSERT INTO `nilo`.`delito`(`pk_nombre`, `descripcion`)
VALUES
('Lesiones Culposas Personales','DESC'),
('Homicidio Culposo','DESC'),
('Lesiones Culposas Personales','DESC'),
('Responsabilidad Extracontractual','DESC');

-- insert documento
INSERT INTO `nilo`.`documento`(`nombre`,`carpeta`,`descripcion`)
VALUES
('ACTUACI�N PROCESAL','Acta de conciliaci�n extraprocesal_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Acta de inasistencia del citado_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Audiencia entrega de Veh�culo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Autoadmisorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Autoinadmisorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Certificado de existencia y Representaci�n Legal','DESC'),
('ACTUACI�N PROCESAL','Certificado de Tradici�n','DESC'),
('ACTUACI�N PROCESAL','Citaci�n diligencia de interrogatorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Citaci�n para Noticificarse de Demanda_YYYYMMDD ','DESC'),
('ACTUACI�N PROCESAL','Constancia Asistencia Conciliaci�n_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Constancia Audiencia Con Acuerdo Conciliatorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Constancia Audiencia de Conciliaci�n_YYYYMMD','DESC'),
('ACTUACI�N PROCESAL','Constancia Audiencia No Acuerdo Conciliatorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Constancia de Archivo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Constancia de Interrogatorio_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Constancia entrega de veh�culo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Contestaci�n demanda XXXX_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Contrato de Transacci�n ','DESC'),
('ACTUACI�N PROCESAL','Demanda_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Derecho de petici�n_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Notificaci�n personal de Demanda_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Orden de archivo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Orden entrega del vehiculo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Sentencia Condenatoria_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud Aplazamiento de Audiencia_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud Copia resoluci�n de archivo_YYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud de Archivo por caducidad de la querella_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud de Archivo por Desistimiento_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud de Archivo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud de Conciliaci�n_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud de Medidas Cautelares_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud Entrega Veh�culo_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud llamamiento en garant�a_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Solicitud pruebas Demandante_YYYYMMDD','DESC'),
('ACTUACI�N PROCESAL','Sustituci�n de poder demandante XXXX_YYYYMMDD','DESC'),
('INFORMES','Estudio de Indemnizaci�n de Perjuicios_YYYYMMDD','DESC'),
('INFORMES','Informe de Reclamaci�n de Accidente_YYYYMMDD','DESC'),
('INFORMES','Informe abogado externo_YYYYMMDD','DESC'),
('INFORMES','Informe SPOA_YYYYMMDD','DESC'),
('OTROS','C�dula XXXX','DESC'),
('OTROS','Cobertura poliza_YYYYMMDD','DESC'),
('OTROS','Contrato de trabajo XXXX','DESC'),
('EMAILS','Correo de asignaci�n_YYYYMMDD','DESC'),
('EMAILS','Correo de seguimiento_YYYYMMDD','DESC'),
('OTROS','Facturas otros_YYYYMMDD','DESC'),
('OTROS','P�liza SOAT Placa XXXX','DESC'),
('OTROS','Poliza sudamericana placa XXXX','DESC'),
('OTROS','Reclamaci�n de Perjuicios_YYYYMMDD','DESC'),
('OTROS','Registro civil de Matrimonio','DESC'),
('OTROS','Registro Civil XXXX','DESC'),
('OTROS','Remisi�n de Sura para responder Reclamaci�n','DESC'),
('ANEXOS PROCESO','Auto recurso de Apelaci�n','DESC'),
('ANEXOS PROCESO','Citaci�n Audiencia de conciliaci�n_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Citaci�n Audiencia de Imputaci�n_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Desistimiento del proceso_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Historia Cl�nica_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Informe de tr�nsito_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Informe Medicina Legal XXXX_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Informe T�cnico de Reconstrucci�n de Accidentes de Tr�nsito_NoXXXX','DESC'),
('ANEXOS PROCESO','Matr�cula Mercantil XXXX','DESC'),
('ANEXOS PROCESO','Objeci�n a la reclamaci�n_YYYYMMDD','DESC'),
('ANEXOS PROCESO','Poder XXXX','DESC'),
('ANEXOS PROCESO','Admisi�n de la demanda ante el juzgado_YYYYMMDD','DESC');


-- insert fiscalia
INSERT INTO `nilo`.`fiscalia`(`pk_nombre`,`ciudad`,`direccion`,`telefono` )
VALUES
('NO ASIGNADA','N/A','N/A',0);


-- insert etapasproceso
INSERT INTO `nilo`.`etapa_proceso`(`etapa`,`pk_nombre`,`descripcion`,
VALUES
('PRE-PROCESAL','ASIGNACI�N','DESC');


-- insert persona
INSERT INTO `nilo`.`persona`
(`nombre`,`usuario`,`email`,`fk_tipoiId`,`documento`,`pais`,`ciudad`,`direccion`,`telefono`,`extension`,`celular`)
VALUES
('Andr�s Polan�a','apolan','C�dula Ciudadan�a','101010184566','Colombia','Bogot�','cll 120 47a 06',2130886,'',3138606408);


-- insert tipoCliente
INSERT INTO `nilo`.`tipocliente`(`pk_nombre`,`descripcion`)
VALUES
(<{pk_nombre: }>,
<{dt_creacion: CURRENT_TIMESTAMP}>,
<{descripcion: }>);


-- insert ORGANISMO



-- CREATE  PROCESO




