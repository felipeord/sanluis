<?php get_header(); ?>
<script>var page = "home";</script>
<div id="section-home" class="row">
    <div class="col-xs-12 slide-page" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/img/bkg-4.jpg');">
        <div class="container animated fadeIn">
            <ul>
                <li>
                    <div class="row white" >
                        <h3 id="pageBld-title-0" class="blog-post-title">desde 1995</h3>
                    </div>
                </li>
                <li >
                    <div class="row white border-line-white" >
                        <h2 id="pageBld-title-2" class="blog-post-title"><?php echo get_post_field('post_title', 26) ?></h2>
                    </div>
                </li>
                <li >
                    <div class="row white" style="margin-top: 4px" >
                        <h3 id="pageBld-title-3" class="blog-post-title"><?php echo get_post_field('post_content', 26) ?></h3>
                    </div>
                </li>
            </ul>
        </div>>
    </div>
</div>

<!-- Handle Section: light -->
<div id="light-day" class="row" >
    <div id="blend-day" class="col-xs-12 slide-page" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/img/bkg-1.jpg');">
        <div class="row wow fadeInLeft" >
            <div id="div-light-day" class="col-xs-6" >
                <img id="img-light-day" class="center-block img-light-big"
                     src="<?php echo get_bloginfo('template_url') ?>/img/manana.png"
                     >
            </div>
            <div class="text-light col-xs-5" >
                <?php
                $counter = 0;
                $args = array(
                    category_name => 'light'
                        // Arguments for your query.
                );
                // Custom query.
                $query = new WP_Query($args);
                $pathWP = get_bloginfo('template_url');
                // Check that we have query results.
                if ($query->have_posts()) {
                    // Start looping over the query results.
                    while ($query->have_posts()) {
                        $query->the_post();

                        $name = get_the_title();
                        if ($name == 'Mañana') {
                            echo '<div>';
                            echo '<h1 class="white pst-left">' . get_the_title() . '</h1>';
                            echo '<p class="white pageText-2">' . get_the_content() . '</p>';
                            echo '</div>';
                        }
                        //echo '    <span>Jacob Cummings</span>';
                    }
                }
                // Restore original post data.
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Handle Section: midday -->
<div id="light-midday" class="row" >
    <div id="blend-midday" class="col-xs-12 slide-page" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/img/bkg-2.jpg');">
        <div class="row wow fadeInLeft" >
            <div id="div-light-midday" class="col-xs-6" >
                <img id="img-light-midday" class="center-block img-light-big"
                     src="<?php echo get_bloginfo('template_url') ?>/img/tarde.png"
                     >
            </div>
            <div class="text-light col-xs-5"  >
                <?php
                $counter = 0;
                $args = array(
                    category_name => 'light'
                        // Arguments for your query.
                );
                // Custom query.
                $query = new WP_Query($args);
                $pathWP = get_bloginfo('template_url');
                // Check that we have query results.
                if ($query->have_posts()) {
                    // Start looping over the query results.
                    while ($query->have_posts()) {
                        $query->the_post();

                        $name = get_the_title();
                        if ($name == 'Tarde') {
                            echo '<div>';
                            echo '<h1 id="light-post" class="white pst-left">' . get_the_title() . '</h1>';
                            echo '<p class="white pageText-2">' . get_the_content() . '</p>';
                            echo '</div>';
                        }
                        //echo '    <span>Jacob Cummings</span>';
                    }
                }
                // Restore original post data.
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Handle Section: night -->
<div id="light-night" class="row" >
    <div id="blend-night" class="col-xs-12 slide-page" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/img/bkg-3.jpg');">
        <div class="row wow fadeInLeft" >
            <div id="div-light-night" class="col-xs-6" >
                <img id="img-light-night" class="center-block img-light-big"
                     src="<?php echo get_bloginfo('template_url') ?>/img/noche.png"
                     >
            </div>
            <div class="text-light col-xs-5" >
                <?php
                $counter = 0;
                $args = array(
                    category_name => 'light'
                        // Arguments for your query.
                );
                // Custom query.
                $query = new WP_Query($args);
                $pathWP = get_bloginfo('template_url');
                // Check that we have query results.
                if ($query->have_posts()) {
                    // Start looping over the query results.
                    while ($query->have_posts()) {
                        $query->the_post();

                        $name = get_the_title();
                        if ($name == 'Noche') {
                            echo '<div>';
                            echo '<h1 class="white pst-left">' . get_the_title() . '</h1>';
                            echo '<p class="white pageText-2">' . get_the_content() . '</p>';
                            echo '</div>';
                        }
                        //echo '    <span>Jacob Cummings</span>';
                    }
                }
                // Restore original post data.
                wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
</div>

<!-- Handle Section: Programas -->
<div id="section-programs" class="row wow fadeInUp"  >
    <div  class="col-xs-12" >
        <div class="row divided">
            <h1 id="pageBld-title-2" class="black80">Nuestros Programas</h1>
            <p class="black80 pageText-1">
                Estos son los Programas disponibles en nuestra sede:
            </p>
        </div>
        <?php
        $counter = 0;
        $args = array(
            category_name => 'programas'
                // Arguments for your query.
        );
        // Custom query.
        $query = new WP_Query($args);
        $pathWP = get_bloginfo('template_url');
        // Check that we have query results.
        if ($query->have_posts()) {
            // Start looping over the query results.
            while ($query->have_posts()) {
                $query->the_post();

                if ($counter == 0) { // Case 0: 
                    echo '<div class = "row programas">';
                }
                $counter++;
                $idlabel = get_the_content();
                echo '<img '
                . 'class="li-servicios-big s' . $idlabel . '" '
                . 'onClick="select(&quot;' . $idlabel . '&quot;)" '
                . 'onmouseover="select(&quot;' . $idlabel . '&quot;)"'
                . 'onmouseout="deselect(&quot;' . $idlabel . '&quot;)"'
                . 'src = "' . $pathWP . '/img/programas-' . $counter . '.png" alt = "Programa ' . get_the_title() . '">';
            }
            echo '</div>';
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>
</div>

<!-- Handle Section: Servicios -->
<div class="row wow fadeInUp" id="section-services" >
    <div  class="col-xs-12" style=";text-align: center">
        <div class="row">
            <h1 id="pageBld-title-3" class="black80">Servicios</h1>
        </div>
        <?php
        $counter = 0;
        $args = array(
            category_name => 'servicios',
            'posts_per_page' => '20',
                // Arguments for your query.
        );
        // Custom query.
        $query = new WP_Query($args);
        $pathWP = get_bloginfo('template_url');
        // Check that we have query results.
        if ($query->have_posts()) {
            // Start looping over the query results.
            while ($query->have_posts()) {
                $query->the_post();
                if ($counter == 0) { // Case 0: 
                    echo '<div class = "row">';
                }
                $counter++;

                $tags = wp_get_post_tags($post->ID); //this is the adjustment, all the rest is bhlarsen

                $pg = "";
                foreach ($tags as $tag) {
                    $pg = $pg . " " . $tag->name;
                }
                global $post;
                $post_slug = $post->post_name;

                $idlabel = "sv-" . $pg;

                echo ' <img '
                . 'class="li-servicios ' . $pg . '"'
                . 'src = "' . $pathWP . '/img/' . $post_slug . '.png"  alt = "Servicio ' . get_the_title() . '">';
            }
            echo '</div>';
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>
    </div>
</div>

<!-- Handle Section: contacto -->
<div id="section-contacto" class="row" style=" background-image: url('<?php echo get_bloginfo('template_url') ?>/img/bkg-contact.JPG');" >
    <div class="col-xs-4 "  >
        <a  target="_blank" href="https://www.google.com.co/maps/place/IPS+San+Luis/@4.7091804,-74.0558862,18z/data=!4m5!3m4!1s0x0:0x932ecec991ac8b17!8m2!3d4.7097163!4d-74.0551988?hl=es">
            <h2 style="margin-top:20px;" id="pageBld-title-4" class="white" >Bogotá</h2>
            <h3 id="pageBld-title-3" class="white" ><?php echo get_option('direccion'); ?></h3>
        </a>
    </div>
    <div class="col-xs-4" >
        <a href="tel:<?php echo get_option('telefono'); x?>">
            <h2  id="pageBld-title-4" class="white"  >Teléfono</h2>
            <h3 id="pageBld-title-3" class="white"  >Tel. (+571) <?php echo get_option('telefono'); ?></h3>
        </a>
    </div>
    <div class="col-xs-4" >
        <a href="tel:<?php echo get_option('celular'); ?>">
            <h2  id="pageBld-title-4" class="white"  >Celular</h2>
            <h3 id="pageBld-title-3" class="white"  >Cel. <?php echo get_option('celular'); ?></h3>
        </a>
    </div>
</div>


<!-- Handle Section: habilitados y certificaciones -->
<div id="section-habilitados" class="row wow fadeInUp" >
    <div  class="col-md-12" style=";text-align: center">
        <div class="row divided">
            <h1 id="pageBld-title-2" class="black80" >Habilitados y Certificados</h1>
            <p class="black80 pageText-1" >
                Comprometidos con la calidad y la atención a nuestros usuarios, estos son nuestros servicios habilitados por el Ministerio de salud.
            </p>
        </div>

        <?php
        $counter = 0;
        $args = array(
            category_name => 'habilitados'
                // Arguments for your query.
        );
        // Custom query.
        $query = new WP_Query($args);
        $pathWP = get_bloginfo('template_url');
        // Check that we have query results.

        if ($query->have_posts()) {
            // Start looping over the query results.

            echo ' <div id="Carousel" class="carousel slide">';
            echo ' <ol class="carousel-indicators">
                    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#Carousel" data-slide-to="1"></li>
                </ol>';
            echo '  <div class = "carousel-inner">';
            while ($query->have_posts()) {
                $query->the_post();
                if ($counter == 0) {
                    echo '<div class = "item active">';
                    echo '  <div class = "row">';
                }
                if ($counter == 4 || $counter == 9) {
                    echo '<div class = "item">';
                    echo '  <div class = "row">';
                }

                $counter++;
                global $post;
                $post_slug = $post->post_name;

                echo '<div class="thumbnail li-habilitados">';
                echo '    <img class=" " src="' . $pathWP . '/img/' . $post_slug . '.png"  alt = "Habilitados' . get_the_title() . '">';
                echo '    <div class="text">' . get_the_content() . '</div>';
                echo '</div>';

                if ($counter == 4) {
                    echo '   </div>';
                    echo '</div>';
                }
            }
            echo '   </div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }
        // Restore original post data.
        wp_reset_postdata();
        ?>

        <br>
    </div>
</div>


<div id="section-hablemos" class="row wow fadeInUp"  >
    <div  class="col-xs-12" style=";text-align: center">
        <div class="row ">
            <h1 id="pageBld-title-2" class="black80">¿Le podemos ayudar?</h1>
            <p class="black80 pageText-1">
                No se preocupe, dejenos sus datos y nosotros lo contactamos.
            </p>
        </div>
        <div class="row contacto-home ">
            <?php echo do_shortcode('[contact-form-7 id="189" title="Contact form 1"]'); ?>

        </div>
    </div>
</div>

<?php
get_footer();
