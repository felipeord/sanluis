var preguntas;
var status = 0;
var index = 0;

var nombre;
var correo;
var esApto = "nodefinido"; // nodefinido | enestudio | apto | noapto


$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
        if (esApto === "nodefinido") {
            esApto = "enestudio";
            initApto();
        }

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        //prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}




/**
 * 
 * @returns {undefined}
 */
function initApto() {
    console.log("Inicio apto " + preguntas);
    if (esApto === "nodefinido") {
        clear('all');

    } else if (esApto === "enestudio") {
        clear('all');
        status = 0;
        preguntas[0].estado = true;
        //create();
    }


    if (esApto === "nodefinido") {
        clear('all');


        $('<p>', {
            class: 'xx',
            id: "title-1",
            text: "¿Desea realiazar el pre-dianóstico?"
        }).appendTo('#presentation');


        $("#respuesta-clean").on('click', function () {
            $("#respuesta-clean").animate({
                opacity: 0.0
            }, 800, function () {
                $("#respuesta-clean").remove();
                esApto = "nodefinido";
                esApto = "enestudio";
            });
        }
        );

    } else if (esApto === "enestudio") {
        create();

        $('<div>', {
            class: 'prg-respuesta-1 col-xs-2  btn btn-primary',
            id: "respuesta-si"
        }).appendTo('#section-respuesta');

        $('<p>', {
            class: 'xx',
            text: "Si"
        }).appendTo('#respuesta-si');


        $("#respuesta-si").on('click', function () {
            $("#itm-" + index).animate({
                opacity: 0.0
            }, 800, function () {
                $("#itm-" + index).remove();
                preguntas[index].respuesta = "si";
                //addTop(index);
                plusIndex();
            });
        });

        $('<div>', {
            class: 'prg-respuesta-1  col-xs-2 btn btn-warning',
            id: "respuesta-no"
        }).appendTo('#section-respuesta');

        $("#respuesta-no").on('click', function () {
            $("#itm-" + index).animate({
                opacity: 0.0
            }, 800, function () {
                $("#itm-" + index).remove();
                preguntas[index].respuesta = "no";
                addTop(index);
                plusIndex();
            });
        });

        $('<p>', {
            class: 'xx',
            text: "No"
        }).appendTo('#respuesta-no');

        $('<div>', {
            class: 'prg-respuesta-1 col-xs-2 btn btn-default',
            id: "respuesta-nose"
        }).appendTo('#section-respuesta');

        $("#respuesta-nose").on('click', function () {
            $("#itm-" + index).animate({
                opacity: 0.0
            }, 800, function () {
                $("#itm-" + index).remove();
                preguntas[index].respuesta = "nose";
                addTop(index);
                plusIndex();
            });
        });

        $('<p>', {
            class: 'xx',
            text: "No se"
        }).appendTo('#respuesta-nose');

    } else if (esApto === "apto" || esApto === "noapto") {
        clear('all');
        $('<div>', {
            class: 'prg-respuesta-1 col-xs-2',
            id: "respuesta-clean"
        }).appendTo('#section-respuesta');

        $('<p>', {
            class: 'xx',
            text: "¿Desea realiazar el pre-dianóstico de nuevo?"
        }).appendTo('#respuesta-clean');


        $("#respuesta-clean").on('click', function () {
            $("#respuesta-clean").animate({
                opacity: 0.0
            }, 800, function () {
                $("#respuesta-clean").remove();
                esApto = "nodefinido";
                initApto();
            });
        });

    }
}


/**
 * 
 * @param {
 type} action
 * @returns {undefined}
 */
function clear(action) {
    if (action === 'all') {
        $('#section-apto-top').html('');
        $('#section-apto').html('');
        $('#section-respuesta').html('');

        $("#title-1").remove();

        for (i = index; i < preguntas.length; i++) {

        }
    } else if (action === "si-no") {
        $('#section-respuesta').html('');
    }
}


function create() {
    for (i = index; i < preguntas.length; i++) {
        if (preguntas[i].estado === true) {

            $('<div>', {
                class: 'prg-type-1',
                id: "itm-" + i
            }).appendTo('#section-apto');

            $("#itm-" + i).on('click', function () {
                $(this).animate({
                    opacity: 0.0
                }, 800, function () {
                    $(this).remove();
                    plusIndex();
                });
            });

            $('<p>', {
                text: preguntas[i].name,
                id: 'aaa' + i
            }).appendTo('#itm-' + i);
        }
    }
}


function plusIndex() {
    index++;
    if (index < preguntas.length) {
        preguntas[index].estado = true;
        create();
    } else {
        clear('si-no');
        $('<div>', {
            class: 'prg-type-1',
            id: "itm-"
        }).appendTo('#section-apto');

        $('<p>', {
            class: 'xx',
            text: "Ha finalizado! Por favor continue en el proceos:",
            id: 'aaa' + i
        }).appendTo("#itm-");

        soyApto();
        
        $("#itm-").on('click', function () {
            $('#modal-apto').modal('toggle');
        });
    }
}


/**
 * 
 * @param {
 type} index
 * @returns {undefined}
 */
function addTop(idx) {

    $('<div>', {
        class: 'prg-type-2',
        id: "itm-top-" + idx
    }).appendTo('#section-apto-top');

    $('<p>', {
        text: preguntas[index].name + " - " + preguntas[index].respuesta,
    }).appendTo("#itm-top-" + idx);


}


function soyApto() {

    esApto = "Apto";
    document.getElementById('btn-eval').setAttribute('style', 'visibility:visible');
    $("#evaluacion").text("Es apto");
    
}


function save() {
    
    nombre = document.getElementById("nombre").value;
    correo = document.getElementById("correo").value;
}