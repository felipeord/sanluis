/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var actualWidth = $(window).width();
var actualHeight;
var actualSize = "normal";
init();

function init() {
    var d = new Date();
    var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
    // document.getElementById("date").innerHTML = d.getFullYear() + "." + months[d.getMonth()] + "." + d.getDate();

    actualWidth = $(window).width();
    if (actualWidth < 420) {
        actualSize = "small";
        resize();
    } else {
        actualSize = "normal";
    }
    resize();


}


var color_pgccp = "#2c3f52";
var color_pgcpo = "#faaf3b";
var light = 'normal';
var moving;

var positionDay = $("#light-day").position().top;
var positionMidday = $("#light-midday").position().top;
var positionNight = $("#light-night").position().top;
var positionProgramas = $("#section-programs").position().top;
var positionServices = $("#section-services").position().top;
var positionHabilitados = $("#section-services").position().top;

var moveNormal = true;
var moveDay = true;
var moveMid = true;
var moveNight = true;
var moveService = true;
var moveProgram = true;
var offset = 200;
$(window).on("scroll touchmove", function () {
    var position = $(document).scrollTop();

    if (actualSize === "small") {
        position = position + 300;
    }
//------- Handle Section: day
    if (position < $("#light-day").position().top - 30) {
        light = 'normal';
        $("#light-day").removeClass("light-day-blend").addClass("light-normal-blend");
        $("#light-midday").removeClass("light-day-blend").addClass("light-normal-blend");
        $("#light-night").removeClass("light-day-blend").addClass("light-normal-blend");
        ;
    }
    ;
    if (position >= $("#light-day").position().top - 150) {
        light = 'day';
        $("#light-day").removeClass("light-midday-blend").addClass("light-day-blend");
        $("#light-midday").removeClass("light-midday-blend").addClass("light-day-blend");
        $("#light-night").removeClass("light-midday-blend").addClass("light-day-blend");
    }
    ;
//------- Handle Section: Midday
    if (position >= $("#light-midday").position().top - 100) {
        light = 'midday';
        $("#light-day").removeClass("light-night-blend").addClass("light-midday-blend");
        $("#light-midday").removeClass("light-night-blend").addClass("light-midday-blend");
        $("#light-night").removeClass("light-night-blend").addClass("light-midday-blend");
        $("#light-day").removeClass("light-day-blend").addClass("light-midday-blend");
        $("#light-midday").removeClass("light-day-blend").addClass("light-midday-blend");
        $("#light-night").removeClass("light-day-blend").addClass("light-midday-blend");
    }
    ;
//------- Handle Section:  night
    if (position >= $("#light-night").position().top - 100) {
        light = 'night';
        $("#light-day").removeClass("light-midday-blend").addClass("light-night-blend");
        $("#light-midday").removeClass("light-midday-blend").addClass("light-night-blend");
        $("#light-night").removeClass("light-midday-blend").addClass("light-night-blend");
    }
    ;
    // Scroll-TO Functionality
    if (position > (0) && position < (0 + offset) && moveNormal) {
        if (actualSize === "normal") {
            document.querySelector('#home').scrollIntoView({behavior: 'smooth'});
        }
        light = 'normal';
        moveDay = true;
        moveMid = true;
        moveNight = true;
        moveNormal = false;
        moveProgram = true;
    } else if (position > (positionDay - offset) && position < (positionDay + offset) && moveDay) {
        light = 'day';
        if (actualSize === "normal") {
            document.querySelector('#light-day').scrollIntoView({behavior: 'smooth'});
        }
        moveDay = false;
        moveMid = true;
        moveNight = true;
        moveNormal = true;
        moveProgram = true;
    } else if (position > (positionMidday - offset) && position < (positionMidday + offset) && moveMid) {
        if (actualSize === "normal") {
            document.querySelector('#light-midday').scrollIntoView({behavior: 'smooth'});
        }
        light = 'midday';
        moveDay = true;
        moveMid = false;
        moveNight = true;
        moveNormal = true;
        moveProgram = true;
    } else if (position > (positionNight - offset) && position < (positionNight + offset) && moveNight) {
        if (actualSize === "normal") {
            document.querySelector('#light-night').scrollIntoView({behavior: 'smooth'});
        }
        console.log("in night");
        light = 'night';
        moveDay = true;
        moveMid = true;
        moveNight = false;
        moveNormal = true;
        moveProgram = true;
    } else if (position > (positionProgramas - offset) && position < (positionProgramas + offset) && moveProgram) {
        if (actualSize === "normal") {
            document.querySelector('#section-programs').scrollIntoView({behavior: 'smooth'});
        }
        light = "normal";
        console.log("in service0" + light);
        moveNormal = true;
        moveDay = true;
        moveMid = true;
        moveNight = true;
        moveProgram = false;
    } else if (position >= positionProgramas - offset) {
        light = "none";
    }

// Menu
    if (light === 'normal') {
        $("#menu").removeClass("top-ow-white").addClass("top-ow-black");
        $(".col-white").removeClass("col-white").addClass("col-black");
        $('#img-logo').attr('src', urlcanonical + "/img/logo-white.png");
        $(".text-menu a").removeClass("whiteblack80").addClass("white");
    } else if (light === 'day' || light === 'midday' || light === 'night') {
        $("#menu").removeClass("top-ow-black").addClass("top-ow-white");
        $(".col-black").removeClass("col-black").addClass("col-white");
        $("#img-logo").css({
            url: urlcanonical + '/img/logos_solo.png',
            transition: "1000ms"
        });
        $('#img-logo').attr('src', urlcanonical + "/img/logos_solo.png");
        $(".text-menu a").removeClass("whiteblack80").addClass("white");
    } else if (light === 'none') {

        $(".text-menu a").removeClass("white").addClass("black80");
        $("#menu").removeClass("top-ow-black").addClass("top-ow-white");
        $(".col-black").removeClass("col-black").addClass("col-white");
        $('#img-logo').attr('src', urlcanonical + "/img/logos_solo.png");
    }
});
/**
 * 
 * @param {type} element
 * @returns {undefined}
 */
function select(element) {
    console.log("Select " + element);
    var color;
    //$('#li-sv-1').mouseenter().mouseleave();
    if (element === "pg-cpo") {
        color = color_pgccp;
    } else if (element === "pg-ccp") {
        color = color_pgcpo;
    } else {
        console.log("Programa no definido. " + element);
    }

    $("." + element).css({
        backgroundColor: color,
        transition: "1000ms"
    });
}

/**
 * 
 * @param {type} element
 * @returns {undefined}
 */
function deselect(element) {
    console.log("Deselect " + element);
    $("." + element).css({
        backgroundColor: "white"
    });
}



function moveToSection(action) {
    if (action === "section-1") {
        moveDay = true;
        moveMid = false;
        moveNight = false;
        moveNormal = false;
        document.querySelector('#light-day').scrollIntoView({behavior: 'smooth'});
    } else if (action === "section-2") {
        document.querySelector('#section-programs').scrollIntoView({behavior: 'smooth'});
        moveDay = false;
        moveMid = false;
        moveNight = false;
        moveNormal = false;
    } else if (action === "section-3") {
        document.querySelector('#section-contacto').scrollIntoView({behavior: 'smooth'});
        moveDay = false;
        moveMid = false;
        moveNight = false;
        moveNormal = false;
    }
}



$(window).resize(function () {
    actualWidth = $(window).width();
    console.log("resize " + actualWidth);
    if (actualSize < 420) {
        if (actualSize === "normal") {
            actualSize = "small";
            resize();
        }

    } else {
        if (actualSize === "small") {
            actualSize = "normal";
            resize();
        }
    }

});


// - - - - -- - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - -

$(document).ready(function () {
    $('#Carousel').carousel({
        interval: 5000
    })
});
// - - - - -- - - - - - - - - - - - - - - - - - - - - -- - - - - - - - - - - - - -




function resize() {
    if (actualSize === "small") {
        // - - - all size
        $("<style>")
                .prop("type", "text/css")
                .html("\
         .res-sm {\
        width:" + actualWidth + "px;\
        }").appendTo("head");

        $("#menu").addClass("res-sm");
        $("#footer-1").addClass("res-sm");
        $("#coll").removeClass("menu-big");

        $("#light-day").addClass("res-sm");
        $("#light-midday").addClass("res-sm");
        $("#light-night").addClass("res-sm");
        $("#section-programs").addClass("res-sm");
        $("#section-services").addClass("res-sm");
        $("#section-contacto").addClass("res-sm");
        $("#section-habilitados").addClass("res-sm");
        $("#section-hablemos").addClass("res-sm");
        
        $("#home").addClass("res-sm");
        $(".wpcf7").addClass("res-sm");

        $(".wpcf7").width(actualWidth-100);

        // -- image light
        $("#div-light-day").removeClass("col-xs-6").addClass("row");
        $("#img-light-day").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-day').attr('src', urlcanonical + "/img/light-manana.png");

        $("#text-light-day").removeClass("col-xs-5").addClass("col-xs-8");

        $("#div-light-midday").removeClass("col-xs-6").addClass("row");
        $("#img-light-midday").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-midday').attr('src', urlcanonical + "/img/light-tarde.png");

        $("#text-light-midday").removeClass("col-xs-5").addClass("col-xs-8");

        $("#div-light-night").removeClass("col-xs-6").addClass("row");
        $("#img-light-night").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-night').attr('src', urlcanonical + "/img/light-night.png");

        $("#text-light-night").removeClass("col-xs-5").addClass("col-xs-8");

    } else if (actualSize === "normal") {
        $("#div-light-day").removeClass("row").addClass("col-xs-6");
        $("#img-light-day").removeClass("img-light-sm").addClass("img-light-big");
        $('#img-light-day').attr('src', urlcanonical + "/img/manana.png");


        $("#text-light-day").removeClass("col-xs-8").addClass("col-xs-5");
    }
}