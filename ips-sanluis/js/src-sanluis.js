/*
 Description:  IPS San Luis Javascript
 Author:       apolanworks
 Version:      1.1
 */


var moveNormal = false;
var moveDay = true;
var moveMid = false;
var moveNight = false;
var moveService = false;
var moveProgram = false;
var offset = 200;


var actualWidth;
var actualHeight;
var actualSize = "normal";
var color_pgccp = "#2c3f52";
var color_pgcpo = "#faaf3b";


var positionDay = "";
var positionMidday = "";
var positionNight = "";
//var positionProgramas = $("#section-programs").position().top;
//var positionServices = $("#section-services").position().top;
//var positionHabilitados = $("#section-services").position().top;


init();


/*
 * 
 */
function init() {
    var d = new Date();
    var months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

    // document.getElementById("date").innerHTML = d.getFullYear() + "." + months[d.getMonth()] + "." + d.getDate();
    loadFont("MyriadPro-BoldCondIt", "mbci");
    loadFont("MyriadPro-Bold", "mpb");
    loadFont("MyriadPro-Regular", "mpr");
    loadFont("MyriadPro-SemiboldSemiCnIt.otf", "mpssi");
    loadFont("Proxima Nova Li.otf", "pnl");


    actualWidth = $(window).width();
    if (actualWidth < 480) {
        actualSize = "small";
        resize();
    } else {
        actualSize = "normal";
    }
    resize();
    if (page === "home") {
        positionDay = $("#light-day").position().top;
        positionMidday = $("#light-midday").position().top;
        positionNight = $("#light-night").position().top;
    }else if (page === "agenda") {
    
    }
}





function loadFont(name, family) {
    var newStyle = document.createElement('style');
    newStyle.appendChild(document.createTextNode("\
    @font-face {\
        font-family: " + family + ";\
        src: url(" + urlcanonical + '/fonts/' + name + '.otf)' + ";\
    }\
    "));
    document.head.appendChild(newStyle);
}

/**
 * 
 * @returns {undefined}
 */
function resize() {
    if (actualSize === "small") {
        // -- image light
        $("#div-light-day").removeClass("col-xs-6").addClass("row");
        $("#img-light-day").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-day').attr('src', urlcanonical + "/img/light-manana.png");

        $("#div-light-midday").removeClass("col-xs-6").addClass("row");
        $("#img-light-midday").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-midday').attr('src', urlcanonical + "/img/light-tarde.png");

        $("#div-light-night").removeClass("col-xs-6").addClass("row");
        $("#img-light-night").removeClass("img-light-big").addClass("img-light-sm");
        $('#img-light-night').attr('src', urlcanonical + "/img/light-night.png");

        // All post
        $(".text-light").removeClass("col-xs-5").addClass("col-xs-12");
        $(".pst-left").removeClass("pst-left").addClass("pst-center");
    }
}



/**
 * 
 * @param {type} action
 * @returns {undefined}
 */
function moveToSection(action) {
    if (action === "section-1") {
        moveDay = true;
        moveMid = true;
        moveNight = true;
        moveNormal = true;
        document.querySelector('#light-day').scrollIntoView({behavior: 'smooth'});
        moveDay = true;
        moveMid = true;
        moveNight = true;
        moveNormal = true;

    } else if (action === "section-2") {
        document.querySelector('#section-programs').scrollIntoView({behavior: 'smooth'});
        moveDay = false;
        moveMid = false;
        moveNight = false;
        moveNormal = false;
    } else if (action === "section-3") {
        document.querySelector('#section-contacto').scrollIntoView({behavior: 'smooth'});
        moveDay = false;
        moveMid = false;
        moveNight = false;
        moveNormal = false;
    }
}



$(window).on("scroll touchmove", function () {
    var position = $(document).scrollTop();

    if (actualSize === "small") {
        position = position + 300;
    }

    if (position < $("#light-day").position().top - 30) {
        blendLight('normal');
    }
    if (position >= $("#light-day").position().top - 150 && position < $("#light-midday").position().top) {
        blendLight('day');
    }

    if (position >= $("#light-midday").position().top - 100 && position < $("#light-night").position().top) {
        blendLight('midday');
    }
    if (position >= $("#light-night").position().top - 100) {
        blendLight('night');
    }


// Scroll-TO Functionality
    if (position > (0) && position < (0 + offset) && moveNormal) {
        light = "normal";
        resetScroll();
        if (actualSize === "normal") {
            document.querySelector('#home').scrollIntoView({behavior: 'smooth'});
        }

    }
    if (position >= (positionDay - offset) && position < (positionDay + offset) && moveDay) {
        console.log("1");
        light = "day";
        blendLight('day');
        resetScroll();
        if (actualSize === "normal") {
            document.querySelector('#light-day').scrollIntoView({behavior: 'smooth'});
        }
    }
    if (position >= (positionMidday - offset) && position < (positionMidday + offset) && moveMid) {
        console.log("2");
        light = "midday";
        blendLight('midday');
        resetScroll();
        if (actualSize === "normal") {
            document.querySelector('#light-midday').scrollIntoView({behavior: 'smooth'});
        }
    }
    if (position >= (positionNight - offset) && position < (positionNight + offset) && moveNight) {
        light = "night";
        blendLight('night');
        resetScroll();

        if (actualSize === "normal") {
            document.querySelector('#light-night').scrollIntoView({behavior: 'smooth'});
        }
    }

// Menu
    if (light === 'normal') {
        $("#menu").removeClass("top-ow-white").addClass("top-ow-black");
        $(".col-white").removeClass("col-white").addClass("col-black");
        $('#img-logo').attr('src', urlcanonical + "/img/logo-white.png");
        $(".text-menu a").removeClass("whiteblack80").addClass("white");
    } else if (light === 'day' || light === 'midday' || light === 'night') {
        $("#menu").removeClass("top-ow-black").addClass("top-ow-white");
        $(".col-black").removeClass("col-black").addClass("col-white");
        $("#img-logo").css({
            url: urlcanonical + '/img/logos_solo.png',
            transition: "1000ms"
        });
        $('#img-logo').attr('src', urlcanonical + "/img/logos_solo.png");
        $(".text-menu a").removeClass("whiteblack80").addClass("white");
    } else if (light === 'none') {

        $(".text-menu a").removeClass("white").addClass("black80");
        $("#menu").removeClass("top-ow-black").addClass("top-ow-white");
        $(".col-black").removeClass("col-black").addClass("col-white");
        $('#img-logo').attr('src', urlcanonical + "/img/logos_solo.png");
    }



});








function blendLight(typeLight) {
    if (typeLight === "normal") { // Caso 0. Quita todas las luces
        $("#blend-day").removeClass("light-day-blend").addClass("light-normal-blend");
        $("#blend-midday").removeClass("light-day-blend").addClass("light-normal-blend");
        $("#blend-night").removeClass("light-day-blend").addClass("light-normal-blend");
        light = typeLight;
    } else if (typeLight === "day") { //
        $("#blend-day").removeClass("light-normal-blend").addClass("light-day-blend");
        $("#blend-midday").removeClass("light-normal-blend").addClass("light-day-blend");
        $("#blend-night").removeClass("light-normal-blend").addClass("light-day-blend");

        $("#blend-day").removeClass("light-midday-blend").addClass("light-day-blend");
        $("#blend-midday").removeClass("light-midday-blend").addClass("light-day-blend");
        $("#blend-night").removeClass("light-midday-blend").addClass("light-day-blend");
        light = typeLight;
    } else if (typeLight === "midday") { //  
        $("#blend-day").removeClass("light-night-blend").addClass("light-midday-blend");
        $("#blend-midday").removeClass("light-night-blend").addClass("light-midday-blend");
        $("#blend-night").removeClass("light-night-blend").addClass("light-midday-blend");

        $("#blend-day").removeClass("light-day-blend").addClass("light-midday-blend");
        $("#blend-midday").removeClass("light-day-blend").addClass("light-midday-blend");
        $("#blend-night").removeClass("light-day-blend").addClass("light-midday-blend");
        light = typeLight;
    } else if (typeLight === "night") { //  

        $("#blend-day").removeClass("light-midday-blend").addClass("light-night-blend");
        $("#blend-midday").removeClass("light-midday-blend").addClass("light-night-blend");
        $("#blend-night").removeClass("light-midday-blend").addClass("light-night-blend");

        $("#blend-day").removeClass("light-day-blend").addClass("light-night-blend");
        $("#blend-midday").removeClass("light-day-blend").addClass("light-night-blend");
        $("#blend-night").removeClass("light-day-blend").addClass("light-night-blend");
        light = typeLight;
    }
}


/**
 * 
 * @returns {undefined}
 */
function resetScroll() {
    moveDay = true;
    moveMid = true;
    moveNight = true;
    moveNormal = true;
    moveProgram = true;
    console.log("reset " + light);

    if (light === "normal") {
        moveNormal = false;
        moveProgram = false;
    } else if (light === "day") {
        moveDay = false;
    } else if (light === "midday") {
        moveMid = false;
    } else if (light === "night") {
        moveNight = false;
    } else if (light === "none") {
        moveNight = false;
    }
}






/**
 * 
 * @param {type} element
 * @returns {undefined}
 */
function select(element) {
    console.log("Select " + element);
    var color;
    //$('#li-sv-1').mouseenter().mouseleave();
    if (element === "pg-cpo") {
        color = color_pgccp;
    } else if (element === "pg-ccp") {
        color = color_pgcpo;
    } else {
        console.log("Programa no definido. " + element);
    }

    $("." + element).css({
        backgroundColor: color,
        transition: "1000ms"
    });
}

/**
 * 
 * @param {type} element
 * @returns {undefined}
 */
function deselect(element) {
    console.log("Deselect " + element);
    $("." + element).css({
        backgroundColor: "white"
    });
}