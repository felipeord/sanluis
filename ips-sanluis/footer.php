

<div id="footer-1" class="row bottom-ow " >
    <div>
        <div class="col-fo col-xs-3"  >
            <p class="txtfooter" style="text-align: center; cursor: pointer;" data-toggle="modal" data-target="#estados"><span style="color:#e7e7e7">.:Estados Financieros:.</span></a></p>
        </div>
        <div class="col-fo col-xs-6"  >
            <p class="txtfooter" style="text-align: center"><span style="color:#e7e7e7">© 2017 - Todos los derechos Reservados. IPS San Luis       NIT <?php echo get_option('nit'); ?></span></p>
        </div>
        <div class="col-fo col-xs-3">
            <p class="txtfooter" style="text-align: center"><span style="color:#8E8E8E">Realizado por: </span><a id="link-apolan" href="http://apolanworks.org/apolan/index.php" target="_blank">apolanworks</a></p>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="estados" class="modal fade" style="margin-top: 10%" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Estados Financieros San Luis</h4>
            </div>
            <div class="modal-body">
                <p>A continuación  los estados financieros.</p>
            </div>
            <div class="container">
                <?php
                $counter = 0;
                $args = array(
                    category_name => 'financiero'
                        // Arguments for your query.
                );
                // Custom query.
                $query = new WP_Query($args);
                $pathWP = get_bloginfo('template_url');
                // Check that we have query results.
                if ($query->have_posts()) {
                    // Start looping over the query results.
                    while ($query->have_posts()) {
                        $query->the_post();
                        $name = get_the_title();
                        echo get_the_content();
                        //echo '    <span>Jacob Cummings</span>';
                    }
                }
                // Restore original post data.
                wp_reset_postdata();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php wp_footer(); ?>
</body>
</html>


