<?php get_header(); ?>

<div class="container text-center" style="margin-top: 150px">
    <div class="row center-block text-center" >
        <?php echo do_shortcode('[ea_bootstrap]'); ?>
    </div>
</div>
<?php
if (have_posts()) : while (have_posts()) : the_post();
        get_template_part('content', get_post_format());
        $pagename = get_query_var('pagename');
        $contentPage = get_the_content();
        ?>
        <script>var page = <?php echo '"' . $pagename . '"' ?>;</script>
        <?php
        
        if ($pagename == "agenda") {

//            do_shortcode($contentPage);
        } else {
            echo "" . $pagename;
        }
    endwhile;
endif;
?>

<?php get_footer(); ?>