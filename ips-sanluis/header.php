
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo get_bloginfo('name'); ?></title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

        <!--     Custom Fonts -->
        <script type="text/javascript">
            var urlcanonical = "<?php echo get_bloginfo('template_url') ?>" + "";
        </script>
        <script src="<?php echo get_bloginfo('template_directory'); ?>/js/wow.js"></script>
        <script>
            new WOW().init();
        </script>


        <?php wp_head(); ?>

    </head>

    <body>
        <nav id="menu" class="top-ow-black navbar-fixed-top visible" >
            <div class="row" >
                <div class="col-xs-2 col-black">
                    <a href="<?php echo esc_url( home_url( '/' ) );?>" >
                        <img id="img-logo" class="center-block logo-black"
                             src="<?php echo get_bloginfo('template_url') ?>/img/logo-white.png" 
                             onclick="<?php echo home_url(); ?>"
                             >
                    </a>
                </div>
                <div id="col-fb" class="col-xs-1 col-black"  >
                    <a href="<?php echo get_option('fb'); ?>">
                        <img  id="img-fb"
                              src="<?php echo get_bloginfo('template_url') ?>/img/logo-face.png"
                              >
                    </a>
                </div>

                <div id="coll" class="col-xs-7 col-black menu-big">
                    <div id="navbarCollapse" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="text-menu "><a class="white" onclick="moveToSection('section-1')" >¿Por qué San Luis?</a></li>
                            <!-- <li class="text-menu "><a class="white" onclick="initApto()" data-toggle="modal" data-target=".bs-example-modal-lg" >¿Soy apto?</a></li>
                            -->
                            <li class="text-menu "><a class="white" onclick="moveToSection('section-2')" >Servicios</a></li>
                            <li class="text-menu "><a class="white" onclick="moveToSection('section-3')">Contacto</a></li>
                            <li class="text-menu "><a class="white" href="<?php echo get_permalink(get_page_by_title('Agenda')); ?>" >Agenda</a></li>
                            <li class="text-menu ">
                                <a class="white" target="_blank" href="<?php echo get_option('fb'); ?>">
                                    <img class="center-block logo" 
                                         src="<?php echo get_bloginfo('template_url') ?>/img/logo-face.png"
                                         onmouseover="this.setAttribute('src', '<?php echo get_bloginfo('template_url') ?>/img/logo-face-color.png');"
                                         onmouseout="this.setAttribute('src', '<?php echo get_bloginfo('template_url') ?>/img/logo-face.png');"
                                         style="height: 20px " >
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>


        <div id="modal-apto" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-body">

                        <?php
                        $counter = 0;
                        $args = array(
                            category_name => 'preguntas'
                        );
                        // Custom query.
                        $script = "";
                        $query = new WP_Query($args);
                        // Check that we have query results.
                        if ($query->have_posts()) {
                            // Start looping over the query results.
                            echo '<script type="text/javascript">';
                            while ($query->have_posts()) {
                                $query->the_post();
                                $contenido = $contenido . '{name: "' . get_the_title() . '", orden: ' . get_post_meta(get_the_ID(), 'order', true) . ', estado: false, respuesta: "none" },';
                                $counter++;
                            }
                            echo 'preguntas = [' . $contenido . '];';
                            echo 'preguntas.sort(function(a,b) {return (a.orden > b.orden) ? 1 : ((b.orden > a.orden) ? -1 : 0);} );';
                            echo '</script>';
                        }



                        // Restore original post data.
                        wp_reset_postdata();
                        ?>

                        <div class="wizard">
                            <div class="wizard-inner wrapper">

                                <ul class="nav nav-tabs" role="tablist">
                                    <li id="presentation" role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" >
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-folder-open"></i>
                                            </span>
                                        </a>

                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" >
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-pencil"></i>
                                            </span>
                                        </a>
                                    </li>
                                    <li role="presentation" class="disabled">
                                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" >
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <form role="form">
                                <div class="tab-content">
                                    <div style="text-align: center" class="tab-pane active" role="tabpanel" id="step1">
                                        <h3 >Nombre</h3>
                                        <input id="nombre" type="nombre" class="form-control" placeholder="Nombre" style="max-width: 30%;display:inline-block;" >
                                        <h3>Correo</h3>
                                        <input id="correo"  type="correo" class="form-control" placeholder="correo"  style="max-width: 30%;display:inline-block;">
                                        <h3> </h3>
                                        <button type="button" class="btn btn-primary next-step" onclick="save();" >Siguiente</button>
                                    </div>
                                    <div style="text-align: center" class="tab-pane" role="tabpanel" id="step2">
                                        <h3>Preguntas</h3>
                                        <div id="section-apto" ></div>
                                        <div id="section-respuesta" class="row"></div>
                                        <ul class="list-inline pull-right">
                                            <li><button id="btn-eval" type="button" style="visibility: hidden;" class="btn btn-primary next-step">Evaluar</button></li>
                                        </ul>
                                    </div>
                                    <div style="text-align: center" class="tab-pane" role="tabpanel" id="complete">
                                        <h3>Ha terminado</h3>
                                        <p id="evaluacion"></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- Modal -->




