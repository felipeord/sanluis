<?php

/**
 * Load scripts
 *
 * @uses wp_enqueue_script action
 */
function load_jquery() {

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, true);
}

add_action('wp_enqueue_script', 'load_jquery');

function scripts() {
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.7');
    wp_enqueue_style('basic-css', get_template_directory_uri() . '/style.css', array(), '3.3.7');
//    wp_enqueue_style('service-css', get_template_directory_uri() . '/css/service.css', array(), '3.3.7');
    wp_enqueue_style('habilitas-css', get_template_directory_uri() . '/css/habilitados.css', array(), '3.3.7');
    wp_enqueue_style('animate-css', get_template_directory_uri() . '/css/animate.css', array(), '3.3.7');
    wp_enqueue_style('form-css', get_template_directory_uri() . '/css/form.css', array(), '3.3.7');
    wp_enqueue_style('wizard-css', get_template_directory_uri() . '/css/wizard.css', array(), '1');

    wp_enqueue_script('fonts', 'https://use.fontawesome.com/fc37eb67c4.js', false, '', false);
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '3.3.7', true);
    wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array('jquery'), '1', false);
    wp_enqueue_script('basic-script', get_template_directory_uri() . '/js/src-sanluis.js', array('jquery', 'bootstrap'), '1', true);
    wp_enqueue_script('apto-script', get_template_directory_uri() . '/js/src-apto.js', array('jquery', 'bootstrap'), '1', true);
    wp_enqueue_script('basic-script-analytic', get_template_directory_uri() . '/js/analytic.js', false, '1', true);
}

add_action('wp_enqueue_scripts', 'scripts');

/*
  // Add scripts and stylesheets
  function startwordpress_scripts() {*

  wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.7');
  wp_enqueue_style('font-awe', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array(), '');
  wp_enqueue_style('styleSL', get_template_directory_uri() . '/style.css', array(), '');
  wp_enqueue_style('service', get_template_directory_uri() . '/css/service.css', array(), '');
  wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array(), '');
  // wp_enqueue_style('modernzr', get_template_directory_uri() . '/css/modernizr.custom.css', array(), '');



  /*
  wp_enqueue_script('jq', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(''), '', true);
  wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array(''), '', false);
  wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array(''), '', false);
  wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', array(''), '', false);


  wp_enqueue_script('jqueryui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js', array(''), '', true);
  wp_enqueue_script('scrollreveal', get_template_directory_uri() . '/js/scrollreveal.js', array(''), '', true);
  wp_enqueue_script('analytic', get_template_directory_uri() . '/js/analytic.js', array(''), '', true);
  wp_enqueue_script('toucheffects', get_template_directory_uri() . '/js/toucheffects.js', array(''), '', true);+

  wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.3.7');
  wp_enqueue_style('blog', get_template_directory_uri() . '/style.css');

  wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array(''), '', false);
  wp_enqueue_script('wow', get_template_directory_uri() . '/js/wow.js', array(''), '', false);
  wp_enqueue_script('jq', get_template_directory_uri() . '/js/jquery-3.2.1.js','' , '', true);
  wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '3.3.7', true);
  wp_enqueue_script('ga', get_template_directory_uri() . '/js/analytic.js', false, '1', true);
  wp_enqueue_script('sanluis', get_template_directory_uri() . '/js/sanluis.js', array('smoothscroll'), '1', true);

  }

  add_action('wp_enqueue_scripts', 'startwordpress_scripts');
 */

// ADD THEME SUPPORT


add_theme_support('title-tag');

// - - - - -- - - - - - - - - - - -  Global Functions
// Custom settings
function add_menu_settings() {
    add_menu_page('mn-sanLuis', 'San Luis Settings', 'manage_options', 'Options', 'custom_settings_page', null, 99);
}

add_action('admin_menu', 'add_menu_settings');

// Create Custom Global Settings
function custom_settings_page() {
    ?>
    <div class="wrap">
        <h1>Custom Settings</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields('section');
            do_settings_sections('theme-options');
            submit_button();
            ?>          
        </form>
    </div>
    <?php
}

// infoBasic
function setting_infoBasic_dir() {
    ?>
    <h1 style=" font-size: 10pt;font-weight: bold; ">Dirección</h1>
    <input type="text" name="direccion" id="sl-dir" value="<?php echo get_option('direccion'); ?>" />
    <?php
}

function setting_infoBasic_tel() {
    ?>
    <h1 style=" font-size: 10pt;font-weight: bold; ">Teléfono</h1>
    <input type="text" name="telefono" id="sl-tel" value="<?php echo get_option('telefono'); ?>" />
    <?php
}

function setting_infoBasic_cel() {
    ?>
    <h1 style=" font-size: 10pt;font-weight: bold; ">Celular</h1>
    <input type="text" name="celular" id="sl-tel" value="<?php echo get_option('celular'); ?>" />
    <?php
}

function setting_infoBasic_nit() {
    ?>
    <h1 style=" font-size: 10pt;font-weight: bold; ">NIT</h1>
    <input type="text" name="nit" id="sl-nit" value="<?php echo get_option('nit'); ?>" />
    <?php
}

function setting_infoBasic_fb() {
    ?>
    <h1 style=" font-size: 10pt;font-weight: bold; ">Facebook</h1>
    <input type="text" name="fb" id="sl-facebook" value="<?php echo get_option('fb'); ?>" />
    <?php
}

function settings_page_setup_info() {
    add_settings_section('section', 'All Settings', null, 'theme-options');

    add_settings_field('direccion', 'Datos generales dir', 'setting_infoBasic_dir', 'theme-options', 'section');
    add_settings_field('telefono', 'Datos generales tel', 'setting_infoBasic_tel', 'theme-options', 'section');
    add_settings_field('celular', 'Datos generales celular', 'setting_infoBasic_cel', 'theme-options', 'section');
    add_settings_field('nit', 'Datos generales nit', 'setting_infoBasic_nit', 'theme-options', 'section');
    add_settings_field('fb', 'Datos generales Facebook', 'setting_infoBasic_fb', 'theme-options', 'section');


    register_setting('section', 'direccion');
    register_setting('section', 'telefono');
    register_setting('section', 'celular');
    register_setting('section', 'nit');
    register_setting('section', 'fb');
}

add_action('admin_init', 'settings_page_setup_info');

function correoTo() {
    ?>        
    $to = 'afpolani@gmail.com';
    $subject = 'Test wordpress ' +  date('Y-m-d H:i:s');
    $message = 'fIXING';

    wp_mail($to, $subject, $message);
    <?php
}


function login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logos_solo.png);
		height: 80px;
		width: 540px;
		background-size: 330px 100px; 
		background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'login_logo' );
